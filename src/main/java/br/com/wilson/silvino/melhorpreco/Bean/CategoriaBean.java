/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.wilson.silvino.melhorpreco.Bean;

import br.com.wilson.silvino.melhorpreco.DAO.CategoriaDAO;
import br.com.wilson.silvino.melhorpreco.Domain.Categoria;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;
import org.omnifaces.util.Messages;

/**
 *
 * @author Wilson
 */
@ManagedBean
@ViewScoped
public class CategoriaBean implements Serializable {

    private Categoria categoria;
    private List<Categoria> categorias;

    public Categoria getCategoria() {
        return categoria;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }

    public List<Categoria> getCategorias() {
        return categorias;
    }

    public void setCategorias(List<Categoria> categorias) {
        this.categorias = categorias;
    }

    public void novo() {
        categoria = new Categoria();
    }

    public void salvar() {
        try {
            CategoriaDAO categoriaDAO = new CategoriaDAO();
            categoriaDAO.merge(categoria);

            categoria = new Categoria();

            categorias = categoriaDAO.listar();

            Messages.addGlobalInfo("Categoria salva com sucesso");
        } catch (RuntimeException erro) {
            Messages.addGlobalError("Ocorreu um erro ao tentar salvar a categoria ");
            erro.printStackTrace();
        }
    }

    @PostConstruct
    public void listar() {
        try {
            CategoriaDAO categoriaDAO = new CategoriaDAO();
            categorias = categoriaDAO.listar("Cadastro Categoria");
        } catch (RuntimeException erro) {
            Messages.addGlobalError("Ocorreu um erro ao tentar listar as Categorias ");
            erro.printStackTrace();
        }
    }

    public void excluir(ActionEvent evento) {
        try {
            categoria = (Categoria) evento.getComponent().getAttributes().get("CategoriaSelecionado");

            CategoriaDAO categoriaDAO = new CategoriaDAO();

            categoriaDAO.excluir(categoria);

            categorias = categoriaDAO.listar();

            Messages.addGlobalInfo("Categoria removida com sucesso");
        } catch (RuntimeException erro) {
            Messages.addFlashGlobalError("Ocorreu um erro ao tentar remover a Categoria ");
            erro.printStackTrace();
        }
    }

    public void editar(ActionEvent evento) {
        categoria = (Categoria) evento.getComponent().getAttributes().get("CategoriaSelecionado");
    }

}

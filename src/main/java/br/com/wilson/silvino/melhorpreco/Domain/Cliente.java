/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.wilson.silvino.melhorpreco.Domain;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import org.hibernate.validator.constraints.Email;

/**
 *
 * @author Wilson
 */
@Entity
public class Cliente extends GenericDomain implements Serializable{
    @Column(nullable = false, length = 100)
    private String nome;
    
    @Column(nullable = false, length = 100)
    private String telefone;
    
    @Column(length = 100, nullable = false)
    @Email (message = "Insira um email valido")
    private String email;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
    
    
}

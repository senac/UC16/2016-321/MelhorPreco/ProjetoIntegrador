/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.wilson.silvino.melhorpreco.Bean;

import br.com.wilson.silvino.melhorpreco.DAO.EstabelecimentoDAO;
import br.com.wilson.silvino.melhorpreco.Domain.Estabelecimento;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;
import org.omnifaces.util.Messages;

/**
 *
 * @author Wilson
 */
@ManagedBean
@ViewScoped
public class EstabelecimentoBean implements Serializable {

    private Estabelecimento estabelecimento;
    private List<Estabelecimento> estabelecimentos;

    public Estabelecimento getEstabelecimento() {
        return estabelecimento;
    }

    public void setEstabelecimento(Estabelecimento estabelecimento) {
        this.estabelecimento = estabelecimento;
    }

    public List<Estabelecimento> getEstabelecimentos() {
        return estabelecimentos;
    }

    public void setEstabelecimentos(List<Estabelecimento> estabelecimentos) {
        this.estabelecimentos = estabelecimentos;
    }

    public void novo() {
        estabelecimento = new Estabelecimento();
    }

    public void salvar() {
        try {
            EstabelecimentoDAO estabelecimentoDAO = new EstabelecimentoDAO();

            estabelecimentoDAO.merge(estabelecimento);

            estabelecimento = new Estabelecimento();

            estabelecimentos = estabelecimentoDAO.listar();

            Messages.addGlobalInfo("Horario salvo com sucesso");
        } catch (RuntimeException erro) {
            Messages.addGlobalError("Ocorreu um erro ao tentar listar os Horarios ");
            erro.printStackTrace();
        }
    }

    @PostConstruct
    public void listar() {
        try {
            EstabelecimentoDAO estabelecimentoDAO = new EstabelecimentoDAO();
            estabelecimentos = estabelecimentoDAO.listar("Cadastro de Horarios");
        } catch (RuntimeException erro) {
            Messages.addGlobalError("Ocorreu um erro ao tentar listar os Horarios ");
            erro.printStackTrace();
        }
    }

    public void excluir(ActionEvent evento) {
        try {

            estabelecimento = (Estabelecimento) evento.getComponent().getAttributes().get("HorarioSelecionado");

            EstabelecimentoDAO estabelecimentoDAO = new EstabelecimentoDAO();
            estabelecimentoDAO.excluir(estabelecimento);

            estabelecimentos = estabelecimentoDAO.listar();

            Messages.addGlobalInfo("Horario removido com sucesso");
        } catch (RuntimeException erro) {
            Messages.addFlashGlobalError("Ocorreu um erro ao tentar remover o Horario ");
            erro.printStackTrace();
        }

    }

    public void editar(ActionEvent evento) {
        estabelecimento = (Estabelecimento) evento.getComponent().getAttributes().get("HorarioSelecionado");
    }

}
